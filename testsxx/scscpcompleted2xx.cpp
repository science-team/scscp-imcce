/*-----------------------------------------------------------------*/
/*! 
  \file scscpcompleted2xx.cpp
  \brief check that client could decode the completed message with complex parse
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016,  M. Gastineau, CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <cstdio>
#include <cstdlib>
#include <iostream>
#if HAVE_STRING_H
#include <cstring>
#endif
#include "scscpxx.h"
#include "scscpsscommon.h"
#include "scscpdebug.h"

using namespace SCSCP;
using namespace std;


/*-----------------------------------------------------------------*/
/* print the answer */
/*-----------------------------------------------------------------*/
static void printelements(Iunfstream& stream, int tab)
{
#define PRINTAB(x) for(j=0; j<x; j++) SCSCP_debugprint(" "); 
 int j;
 OMS symbol;
 OMR href; 
 OMV var; 
 
 while (!stream.eof())
 {
  PRINTAB(tab); 
  const char *name = stream.get_typename();
  SCSCP_debugprint("node : '%s'\n" , name?name:"content node");
  
  for (Iunfstream::iterator_attr attr = stream.get_attr(); attr.end() ; ++attr)
  {
     PRINTAB(tab+1); 
     SCSCP_debugprint("attribute : '%s' '%s'\n",  attr.get_name() , attr.get_value());
  }
  
  const char * content = stream.get_content();
  int parsechild = 0;
  switch(stream.get_type())
  {
    
    case SCSCP_omtype_OMI : int v; stream>> v; SCSCP_debugprint("integer = %d\n", v); break;
    case SCSCP_omtype_OMF : double f; stream>> f; SCSCP_debugprint("floating-point = %g\n",f);break;
    case SCSCP_omtype_OMV :  stream >> var; 
                            SCSCP_debugprint("variable = '%s'\n",  var.get_name());
                            break;
    case SCSCP_omtype_OMS :  stream>> symbol; 
                            SCSCP_debugprint("symbol = name : %s cd : %s\n", symbol.get_symbolname(), symbol.get_cdname()); 
                            break;
    case SCSCP_omtype_OMSTR: const char *s; stream>> s; 
                            SCSCP_debugprint("string = '%s'\n", s);
                            break;
    case SCSCP_omtype_OMB:  SCSCP_debugprint("OMB\n");  parsechild=1; break;
    case SCSCP_omtype_OMFOREIGN: SCSCP_debugprint("OMFOREIGN\n");   parsechild=1; break; 
    case SCSCP_omtype_OMA:  SCSCP_debugprint("OMA\n");   parsechild=1; break;
    case SCSCP_omtype_OMBIND: SCSCP_debugprint("OMBIND\n");   parsechild=1; break;
    case SCSCP_omtype_OMATTR: SCSCP_debugprint("OMATTR\n");   parsechild=1; break;
    case SCSCP_omtype_OME:   SCSCP_debugprint("OME\n");  parsechild=1; break;
    case SCSCP_omtype_OMATP: SCSCP_debugprint("OMATP\n");   parsechild=1; break;
    case SCSCP_omtype_OMOBJ: SCSCP_debugprint("OMOBJ\n");  parsechild=1; break;
    case SCSCP_omtype_OMBVAR: SCSCP_debugprint("OMBVAR\n");  parsechild=1; break;
                            
    case SCSCP_omtype_OMR:  stream>> href; 
                             SCSCP_debugprint("reference = '%s'\n",   href.get_reference()); 
                            break;
    case SCSCP_omtype_CONTENT :SCSCP_debugprint("content node \n");  break;
  }
  
  if (content)
  {
    PRINTAB(tab+1);
    SCSCP_debugprint("content : %s\n", content );
  }
  
  if (parsechild)
  {
   Iunfstream children = stream.beginOM();
   printelements(children,tab+4);
  }
 }
 
}


/*-----------------------------------------------------------------*/
/* decode the answer of the server */
/*-----------------------------------------------------------------*/
static int decodeanswer(Client::Computation& mytask)
{
 int res;
 SCSCP_msgtype msgtype;
 Iunfstream* inputstream;

 res = mytask.recv(msgtype, inputstream);
 
 if (res)
 {
  switch(msgtype)
  {
   case SCSCP_msgtype_ProcedureTerminated : 
        printf("Procedure Terminated=> can't be possible\n");
        res=0;
        break;
   case SCSCP_msgtype_ProcedureCompleted : 
        SCSCP_debugprint("Procedure Completed\n");
        printelements(*inputstream, 0);
        break;
   default :
        printf("unknown message type !!!\n"); 
        res = 0;
        break;
  }
 }
 return res;
}

/*-----------------------------------------------------------------*/
/*  main program */
/*-----------------------------------------------------------------*/
int main()
{
 int reserver;
 int res;
 int port;
 
 res = startbackgroundserver(2, port);
 
 if (res)
 {
  Client client;
  const char *cmd = "<OMA><OMS cd=\"scscp2\" name=\"get_allowed_heads\" /></OMA>";
  size_t len=::strlen(cmd);
  try
  {
   res = client.connect("localhost", port); 
   Client::Computation mytask(client);
   if (res) res = mytask.send(cmd, len, SCSCP_option_return_object);
   if (res) res = decodeanswer(mytask);
   if (res) res = client.close(); 
  }
  catch(Exception e)
  {
   res = 0;
   std::cout<<"SCSCP::Exception "<<e.what()<<std::endl;
  }
 }
 reserver = stopbackgroundserver();

 return !(res==1 && reserver==0);
}
