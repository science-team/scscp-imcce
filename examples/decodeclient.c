/*-----------------------------------------------------------------*/
/*! 
  \file decodeclient.c
  \brief check that client could decode the completed message with complex parse
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <scscp.h>

/*-----------------------------------------------------------------*/
/* print the xml tree */
/*-----------------------------------------------------------------*/
static void printelements(SCSCP_xmlnodeptr node, int tab)
{
#define PRINTAB(x) for(j=0; j<x; j++) putchar(' '); 
 SCSCP_xmlattrptr attr;
 const char *name;
 const char *value;
 const char *content;
 int j;
 
 while (node!=NULL)
 {
  const char *nodename = SCSCP_xmlnode_getname(node);
  PRINTAB(tab); 
  printf ("node : '%s'\n",nodename==NULL?"":nodename);
  
  for (attr = SCSCP_xmlnode_getattr(node); attr!=NULL; attr = SCSCP_xmlattr_getnext(attr))
  {
     SCSCP_xmlattr_getvalue(attr, &name, &value);
     PRINTAB(tab+1); 
     printf ("attribute : '%s'  = '%s'\n",name, value);
  }
  content = SCSCP_xmlnode_getcontent(node);
  if (content)
  {
    PRINTAB(tab+1);
    printf ("content : '%s'\n",content);
  }
  printelements(SCSCP_xmlnode_getchild(node),tab+4);
  node = SCSCP_xmlnode_getnext(node);
 }
 
}

/*-----------------------------------------------------------------*/
/* decode the answer of the server */
/*-----------------------------------------------------------------*/
static int decodeanswer(SCSCP_socketclient *client, SCSCP_status *status)
{
 int res;
 SCSCP_msgtype msgtype;
 SCSCP_returnoptions options;
 res = SCSCP_ro_init(&options, status); 
 if (!res) return res;
 
 res = SCSCP_sc_callrecvheader(client, &options, &msgtype, status);
 if (res)
 {
  switch(msgtype)
  {
   case SCSCP_msgtype_ProcedureTerminated : 
        printf("Procedure Terminated !!!!\n");
        printelements(SCSCP_sc_getxmlnode(client, status), 0);
        break;
   case SCSCP_msgtype_ProcedureCompleted : 
        printf("Procedure Completed !!!\n");
        printelements(SCSCP_sc_getxmlnode(client, status), 0);
        break;
   default :
        printf("unknown message type !!!\n"); 
        res = 0;
        break;
  }
 }
 SCSCP_ro_clear(&options, status);
 return res;
}

/*-----------------------------------------------------------------*/
/*  main program */
/*-----------------------------------------------------------------*/
int main(int argc, char *argv[])
{
 SCSCP_socketclient client;
 SCSCP_status status = SCSCP_STATUS_INITIALIZER;
 const char *host = "localhost";
 int port = SCSCP_PROTOCOL_DEFAULTPORT;
 int res;
 SCSCP_calloptions calloptions;
 
 
 if (argc==3) 
 {
  host = argv[1];
  port = atoi(argv[2]);
 }
 printf("---------- connecting to %s:%d ---------\n", host, port);
 
 /* open connection with the server on port 26133 */
 res = SCSCP_sc_init(&client, &status, 
                    SCSCP_PROTOCOL_VERSION_1_3, SCSCP_PROTOCOL_VERSION_1_2,
                    NULL); 
 if (res) res = SCSCP_sc_connect(&client, host, port, &status); 

 /* send a invalid oma */
 printf ("-----------invalid command----------\n");
 if (res) res = SCSCP_co_init(&calloptions, &status);
 if (res) res = SCSCP_co_set_returntype(&calloptions, SCSCP_option_return_object, &status);
 if (res) res = SCSCP_sc_callsendstr(&client, &calloptions, 
                                     "<OMA><OMS cd=\"scscp2\" name=\"get_allowed_heads\" /></OMA>",
                                     &status); 
 printf ("-----------receive the first answer----------\n");
 /* receive the answer */
 if (res) res = decodeanswer(&client, &status);
 /* some server disconnects after an error.... So we reconnect */
 
 if (res) res = SCSCP_sc_close(&client, &status); 
 if (res) res = SCSCP_sc_connect(&client, host, port, &status); 
 
 /* send a valid symbol */
 printf ("----------- valid command----------\n");
 if (res) res = SCSCP_co_init(&calloptions, &status);
 if (res) res = SCSCP_co_set_returntype(&calloptions, SCSCP_option_return_object, &status);
 if (res) res = SCSCP_sc_callsendstr(&client, &calloptions, 
                                     "<OMS cd=\"scscp2\" name=\"get_allowed_heads\" />",
                                     &status); 
 printf ("-----------receive the second answer----------\n");
 /* receive the answer */
 if (res) res = decodeanswer(&client, &status);

 /* send an invalid symbol */
 printf ("-----------invalid command----------\n");
 if (res) res = SCSCP_sc_callsendstr(&client, SCSCP_CALLOPTIONS_DEFAULT,
                                     "<OMS cd=\"myinvalidcd\" name=\"myinvalidsymbol\" />",
                                     &status); 
 printf ("-----------receive the third answer----------\n");
 /* receive the answer */
 if (res) res = decodeanswer(&client, &status);
 /* close the connection */
 if (res) res = SCSCP_sc_close(&client, &status); 
 SCSCP_sc_clear(&client, &status);
 if (res) res = SCSCP_co_clear(&calloptions, &status); 
 
 if (res==0) printf("status=%s\n", SCSCP_status_strerror(&status));
 SCSCP_status_clear(&status);
 return (res!=1);
}

