/*-----------------------------------------------------------------*/
/*! 
  \file simplestclientstreamfmtxx.cpp
  \brief example : the simplest C++ client using formatted stream !
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <scscpxx.h>

using namespace SCSCP;

/*-----------------------------------------------------------------*/
/*  main program */
/*-----------------------------------------------------------------*/
int main(int argc, char *argv[])
{
 Client myclient;
 const char *openmath = "<OMA><OMS cd=\"scscp2\" name=\"store_session\" /><OMI>6177887</OMI></OMA>";
 char *buffer;
 size_t lenbuffer;
 SCSCP_msgtype msgtype;
 const char *host = "localhost";
 int port = SCSCP_PROTOCOL_DEFAULTPORT;
 int res;
 
 if (argc==3) 
 {
  host = argv[1];
  port = atoi(argv[2]);
 }
 printf("---------- connecting to %s:%d ---------\n", host, port);
 try {
 
 /* open connection with the server on port 26133 */
 res = myclient.connect(host, port);

 printf("---------- procedure call with formatted stream ---------\n");
 /* send the command openmath */
 Client::Computation mytask2(myclient);
 std::ostream* stream;
 mytask2.send(SCSCP_option_return_object, stream);
 *stream << openmath;
 mytask2.finish();
 
 /* receive the answer */
 mytask2.recv(msgtype, buffer,lenbuffer); 
 
 /* print the answer */
  switch(msgtype)
  {
   case SCSCP_msgtype_ProcedureTerminated : 
        printf("Procedure Terminated\n");
        break;
   case SCSCP_msgtype_ProcedureCompleted : 
        printf("Procedure Completed\n");
        break;
        
   default :
        printf("unknown message type !!!\n"); 
        break;
  }
  printf("returned message (%d bytes):\n",(int)lenbuffer);
  printf("--------cut here--------\n");
  puts(buffer);
  printf("--------cut here--------\n");
  
  /* close the connection */
  myclient.close(); 
 }
 catch (Exception e)
 { /* error */
  printf("Exception : %s\n", e.what());
  return 1;
 }
 return (res!=1);
}
