/*-----------------------------------------------------------------*/
/*! 
  \file remoteclient.c
  \brief client send, retrieve and delete a remote object using string
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <scscp.h>

/*-----------------------------------------------------------------*/
/* hook function */
/*-----------------------------------------------------------------*/
static int writeobject(SCSCP_io *stream, void *param, SCSCP_status *status)
{
 return SCSCP_io_writeOMIint(stream, *(int *)param, NULL, status);
}

/*-----------------------------------------------------------------*/
/*  main program */
/*-----------------------------------------------------------------*/
int main(int argc, char *argv[])
{
 SCSCP_socketclient client;
 SCSCP_status status = SCSCP_STATUS_INITIALIZER;
 int n = 3;
 char *cookiename = NULL;
 char *openmathbuffer = NULL;
 const char *host = "localhost";
 int port = SCSCP_PROTOCOL_DEFAULTPORT;
 int res;
 
 if (argc==3) 
 {
  host = argv[1];
  port = atoi(argv[2]);
 }
 printf("---------- connecting to %s:%d ---------\n", host, port);
 
 /* open connection with the server on port 26133 */
 res = SCSCP_sc_init(&client, &status, 
                    SCSCP_PROTOCOL_VERSION_1_3, SCSCP_PROTOCOL_VERSION_1_2,
                    NULL); 
 if (res) res = SCSCP_sc_connect(&client, host, port, &status); 

 /* store the value 3 as a remote object */
 printf ("---------- store the value n ---------\n");
 if (res) res = SCSCP_sc_remoteobjectstoresessionhook(&client, writeobject,
                                     &n, &cookiename, &status); 
 if (res) printf("reference object name = %s\n", cookiename);
 
 /* retrieve its value  */
 printf ("----------- retrieve its value ---------\n");
 if (res) res = SCSCP_sc_remoteobjectretrievestr(&client, cookiename, &openmathbuffer, 
                                                 &status); 
 if (res)
 {
  printf("OpenMath object representation from the server :'%s'\n", openmathbuffer);
  free(openmathbuffer);
 }
 
 /* delete the remote object */
 printf ("----------- delete remote object ----------\n");
 if (res) res = SCSCP_sc_remoteobjectunbind(&client, cookiename, &status); 
 if( cookiename) free(cookiename);

 /* close the connection */
 if (res) res = SCSCP_sc_close(&client, &status); 
 if (res==0) printf("error message=%s\n", SCSCP_status_strerror(&status));
 SCSCP_sc_clear(&client, &status);
 SCSCP_status_clear(&status);
 return (res!=1);
}
