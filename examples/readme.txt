This directory contains some examples to build SCSCP client and server.

C examples
----------

- simplestclient.c : 
The simplest client stores the value 6177887 on the SCSCP server which runs on "localhost" and listens on port 26133
. It prints the returned message.

- decodeclient.c : 
This client decodes each node of the OpenMath expression received from the server.
It requests the allowed procedure names. 
After decoding the first answer, it sends a symbol which doesn't exist in order to produce an error.

- remoteclient.c : 
This client sends the value 3 to the server and store it as a remote object.
It retrieves the value of the remote object.
At end, it deletes the remote object from the server.

- execclient.c : 
This client sends the value 3.0 to the server and store it as a remote object.
It computes and gets the result of "1+n^2" where n is the previous remote object.
At end, it deletes the remote object from the server.
 
- decodeserver.c : 
This server which decodes each node of the OpenMath expression received from the client.
It prints the received OpenMath expression and returns a "procedure completed" message. 


C++ examples
----------

- simplestclientxx.cpp : 
The simplest client stores the value 6177887 on the SCSCP server using the function sendstr/recvstr which runs on "localhost" and listens on port 26133
. It prints the returned message.

- simplestclientstreamfmtxx.cpp : 
The simplest client stores the value 6177887 on the SCSCP server using the formatted stream which runs on "localhost" and listens on port 26133
. It prints the returned message.

- simplestclientstreamunfxx.cpp : 
The simplest client stores the value 435 on the SCSCP server using the unformatted stream which runs on "localhost" and listens on port 26133
. It prints the returned message.

- decodeserverxx.cpp : 
This server which decodes each node of the OpenMath expression received from the client.
It prints the received OpenMath expression and returns a "procedure completed" message. 
