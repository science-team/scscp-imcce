/*-----------------------------------------------------------------*/
/*! 
  \file simplestclient.c
  \brief example : the simplest client !
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <scscp.h>

/*-----------------------------------------------------------------*/
/*  main program */
/*-----------------------------------------------------------------*/
int main(int argc, char *argv[])
{
 SCSCP_status status = SCSCP_STATUS_INITIALIZER;
 SCSCP_socketclient client;
 const char *openmath = "<OMS cd=\"scscp2\" name=\"store_session\" /><OMI>6177887</OMI>";
 char *buffer;
 SCSCP_msgtype msgtype;
 SCSCP_calloptions calloptions;
 const char *host = "localhost";
 int port = SCSCP_PROTOCOL_DEFAULTPORT;
 int res;
 
 if (argc==3) 
 {
  host = argv[1];
  port = atoi(argv[2]);
 }
 printf("---------- connecting to %s:%d ---------\n", host, port);
 
 /* open connection with the server on port 26133 */
 res = SCSCP_sc_init(&client, &status, 
                    SCSCP_PROTOCOL_VERSION_1_3, SCSCP_PROTOCOL_VERSION_1_2,
                    NULL); 
 if (res) res = SCSCP_sc_connect(&client, host, port, &status); 

 /* send the command openmath */
 SCSCP_co_init(&calloptions, &status);
 if (res) SCSCP_co_set_returntype(&calloptions,SCSCP_option_return_cookie, &status);
 if (res) res = SCSCP_sc_callsendstr(&client, &calloptions, openmath, &status); 

 /* receive the answer */
 if (res) res = SCSCP_sc_callrecvstr(&client, SCSCP_RETURNOPTIONS_IGNORE,
                                     &msgtype, &buffer, &status); 

 /* print the answer */
 if (res)
 {
  switch(msgtype)
  {
   case SCSCP_msgtype_ProcedureTerminated : 
        printf("Procedure Terminated\n");
        break;
   case SCSCP_msgtype_ProcedureCompleted : 
        printf("Procedure Completed\n");
        break;
        
   default :
        printf("unknown message type !!!\n"); 
        break;
  }
  printf("returned message :\n");
  printf("--------cut here--------\n");
  puts(buffer);
  printf("--------cut here--------\n");
 }
 
 
 /* close the connection */
 if (res) res = SCSCP_sc_close(&client, &status); 
 SCSCP_sc_clear(&client, &status);
 SCSCP_co_clear(&calloptions, &status);
 
 /* display the error if an error occurs */
 if (res==0) 
 {
  printf("error : %d\n", SCSCP_status_is(&status));
 }
 SCSCP_status_clear(&status);
 return (res!=1);
}
