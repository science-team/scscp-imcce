/*-----------------------------------------------------------------*/
/*! 
  \file scscpremote1.c
  \brief check that client could send, retrieve and delete a remote object using string
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009, 2010, 2011, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <stdio.h>
#include <stdlib.h>
#if HAVE_STRING_H
#include <string.h>
#endif
#include "scscp.h"
#include "scscpsscommon.h"

/*-----------------------------------------------------------------*/
/* hook function */
/*-----------------------------------------------------------------*/
static int writehook(SCSCP_io *stream, void *param, SCSCP_status *status)
{
 return SCSCP_io_writeOMIstr(stream, (const char*)param, NULL, status);
}

/*-----------------------------------------------------------------*/
/*  main program */
/*-----------------------------------------------------------------*/
int main()
{
 SCSCP_socketclient client;
 SCSCP_status status = SCSCP_STATUS_INITIALIZER;
 int res, reserver, port;
 char *cookiename=NULL;
 char *openmathbuffer = NULL;
 SCSCP_xmlnodeptr node = NULL;
 
 res = startbackgroundserver(8, &port);
 
 /* open connection with the server on port  */
 if (res) res = SCSCP_sc_init(&client, &status, SCSCP_PROTOCOL_VERSION_1_3, NULL); 
 if (res) res = SCSCP_sc_connect(&client, "localhost", port, &status); 

 /* store */
 if (res) res = SCSCP_sc_remoteobjectstoresessionhook(&client, writehook, (void*)"2", &cookiename,
                                     &status); 

 /* get str */
 if (res) res = SCSCP_sc_remoteobjectretrievestr(&client, cookiename, &openmathbuffer, &status); 
 if (res && strcmp(openmathbuffer,"<OMI>2</OMI>")!=0)
 {
  printf("received openmath object : '%s'\n", openmathbuffer);
  res = 0;
 }
 if (openmathbuffer) free(openmathbuffer);

 /* get node */
 if (res) res = SCSCP_sc_remoteobjectretrievexmlnode(&client, cookiename, &node, &status); 
 openmathbuffer = SCSCP_sc_getxmlnoderawstring(&client, SCSCP_sc_getxmlnode(&client, &status), &status);
 if (res && strcmp(openmathbuffer,"<OMI>2</OMI>")!=0)
 {
  printf("received openmath object node : '%s'\n", openmathbuffer);
  res = 0;
 }
 if (openmathbuffer) free(openmathbuffer);
 
 /*delete */
 if (res) res = SCSCP_sc_remoteobjectunbind(&client, cookiename, &status); 
 
 /* close the connection */
 if (res) res = SCSCP_sc_close(&client, &status); 
 SCSCP_sc_clear(&client, &status);
 if( cookiename) free(cookiename);
 SCSCP_status_clear(&status);

 reserver = stopbackgroundserver();
 
 return !(res==1 && reserver==0);
}
