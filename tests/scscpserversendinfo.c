/*-----------------------------------------------------------------*/
/*! 
  \file scscpserversendinfo.c
  \brief check that the server sends an info message before its answer.
   \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009, 2010, 2011, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <stdio.h>
#include <stdlib.h>
#include "scscp.h"
#include "scscpsscommon.h"
#include "scscpdebug.h"

/*-----------------------------------------------------------------*/
/* decode the answer of the server */
/*-----------------------------------------------------------------*/
static int decodeanswer(SCSCP_socketclient *client, SCSCP_status *status)
{
 int res;
 SCSCP_msgtype msgtype;
 char *openmathbuffer;

     res =SCSCP_sc_callrecvstr(client, SCSCP_RETURNOPTIONS_IGNORE, &msgtype, &openmathbuffer, status);
    
     if (res)
     {
      switch(msgtype)
      {
       case SCSCP_msgtype_ProcedureTerminated : 
            printf("Procedure Terminated\n");
            res =0;
            break;
       case SCSCP_msgtype_ProcedureCompleted : 
            SCSCP_debugprint("Procedure Completed\n");
            break;
       default : res=0; break;
      }
      SCSCP_debugprint("buffer:'%s'\n",openmathbuffer);
      free(openmathbuffer);
     }
     else
     {
      printf ("SCSCP_sc_callrecvstr : failed status=%d\n", SCSCP_status_is(status));
     }

 return res;
}


/*-----------------------------------------------------------------*/
/* send the command to the server */
/*-----------------------------------------------------------------*/
int mainsend(int port)
{
 SCSCP_socketclient client;
 SCSCP_status status = SCSCP_STATUS_INITIALIZER;
 int res;
 int res1 = SCSCP_sc_init(&client, &status, SCSCP_PROTOCOL_VERSION_1_3, NULL); 
 res = res1;
 if (res) res = SCSCP_sc_connect(&client, "localhost", port, &status); 
 if (res) res = SCSCP_sc_callsendstr(&client, SCSCP_CALLOPTIONS_DEFAULT,
                                     "<OMA><OMS cd=\"scscp2\" name=\"get_allowed_heads\" /></OMA>",
                                     &status); 
 if (res) res = decodeanswer(&client, &status);
 if (res) res = SCSCP_sc_close(&client, &status);
 if (res1) res1 = SCSCP_sc_clear(&client, &status);
 if (res) res1 = res;
 SCSCP_status_clear(&status);
 return res;
}

int main()
{
 int reserver;
 int res;
 int port;
 
 startbackgroundserver(6, &port);
 
 res = mainsend(port);

 reserver = stopbackgroundserver();

 return !(res==1 && reserver==0);
}
