
              a SCSCP C library


Description
-----------

This  is  a  release  of the  SCSCP  C library.  This library  is
developed  in the "Astronomy and Dynamical System" team
at  Observatoire de Paris-IMCCE-CNRS (PARIS).  The contact
person  is Mickael Gastineau ("gastineau@imcce.fr").

This library is an implementation of the  
 Symbolic Computation Software Composibility Protocol (SCSCP).

The sources needed  to build  the Symbolic Computation Software 
Composibility Protocol (SCSCP)  library are in  directory src.

To compile and use this library, 
please refer to the documentation in the directory "doc".




The library is "dual-licensed" (CeCILL-C or CeCILL),
you have to choose one of the two licenses  below to apply on the library.
  
  CeCILL-C

	The CeCILL-C license is close to the GNU LGPL.
    ( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html or COPYING_CECILL_C.LIB)
 
or  
   CeCILL v2.0
    The CeCILL license is compatible with the GNU GPL.
    ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html or COPYING_CECILL_V2.LIB)
    
