/*-----------------------------------------------------------------*/
/*! 
  \file scscpexception.cpp
  \brief Exception implementation
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009,2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/

#include <cstdio>

#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpfileclient.h"
#include "scscpfileserver.h"
#include "scscpprocedurecall.h"
#include "scscpdebug.h"
#include "scscpomdoc.h"
#include "scscpbinary.h"
#include "scscpxmlparser.h"
#include "scscpxx.h"

namespace SCSCP
{
#include "scscpstream.h"

/*-----------------------------------------------------------------*/
/*! constructor
 @param stream (inout) socket stream (not NULL)
 @param status (inout) status error (not NULL)
*/
/*-----------------------------------------------------------------*/
Exception::Exception(const SCSCP_status* status)
{
 m_status = SCSCP_STATUS_INITIALIZER;
 SCSCP_status_copy(&m_status, status);
}
   
/*-----------------------------------------------------------------*/
/*! constructor
 @param e (in) exception
*/
/*-----------------------------------------------------------------*/
Exception::Exception(const Exception& e)
{
 m_status = SCSCP_STATUS_INITIALIZER;
 SCSCP_status_copy(&m_status, &e.m_status);
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
Exception::~Exception() throw ()
{
 SCSCP_status_clear(&m_status);
}
   
/*-----------------------------------------------------------------*/
/*! information about the error
*/
/*-----------------------------------------------------------------*/
const char * Exception::what() const throw()
{
 return SCSCP_status_strerror(&m_status);
}

} /* namespace SCSCP */
