/*-----------------------------------------------------------------*/
/*! 
  \file scscpfileclient.h
  \brief SCSCP client with file descriptor
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#ifndef __SCSCPFILECLIENT_H__
#define __SCSCPFILECLIENT_H__


#include "scscputil.h"
#include "scscpfileio.h"
#include "scscpomdoc.h"

#if defined (__cplusplus)
extern "C" {
#endif /* (__cplusplus) */

/*-----------------------------------------------------------------*/
/*! handle client connection SCSCP with file descriptor
*/
/*-----------------------------------------------------------------*/
typedef struct SCSCP_stFileclient
{
  
   /*-----------------------------------------------------------------*/
   /* attributes */
   /*-----------------------------------------------------------------*/
   SCSCP_fileio m_OutStream;           /*!< flux sortant */
   SCSCP_fileio m_InStream;            /*!< flux entrant */
   char *       m_bufferInput;         /*!< input buffer */
   int          m_bufferInputValid;    /*!< number of valid characters in the input buffer */
   
   SCSCP_string *m_allowedversions; /*!< allowed versions */
   size_t m_allowedversionscount; /*!< number of elements of m_allowedversions */
    
    int          m_recvquit;           /*!< = 1 if  quit message was received */
   
   SCSCP_encodingtype m_sendencodingtype; /*!< encoding used for the sent messages */
   SCSCP_encodingtype m_recvencodingtype; /*!< encoding used for the received messages */

   /* to migrate in a call manager */
   SCSCP_xmldocptr  m_doc;      /*!< xml document */
   SCSCP_xmlnodeptr m_iter;    /*!< current iterator on DOM document */
} SCSCP_Fileclient;



int SCSCP_sc_checkNULL(SCSCP_socketclient* client, SCSCP_status* status);
SCSCP_xmlnodeptr* SCSCP_sc_getxmlnodeptr(SCSCP_socketclient *client, SCSCP_status* status);
int SCSCP_sc_xmlnodereadOMEstr(SCSCP_socketclient* client, SCSCP_xmlnodeptr* curnode, const char **cdname, 
                             const char **symbolname, char **value, SCSCP_status *status);

void SCSCP_sc_freexmldoc(SCSCP_Fileclient* client);
int SCSCP_sc_readxmldoc(SCSCP_Fileclient* client, SCSCP_status *status);

/*! connect to a server at the specified port*/
int SCSCP_sc_connectsocket(SCSCP_Fileclient* client, const char *servername, int port, SCSCP_status* status);
/*! close the connexion to the server */
void SCSCP_sc_closesocket(SCSCP_Fileclient* client);

/*! Attach a socket*/
void SCSCP_sc_setsocket(SCSCP_Fileclient* client, int socketid); 
/*! Get the socket number*/
int SCSCP_sc_getsocket(SCSCP_Fileclient* client); 
   
    /*! read a line from the input stream and fill buffer.*/
int SCSCP_sc_readline(SCSCP_Fileclient* client, char *buffer, int lenbuffer, SCSCP_status *status);
   /*! read the CIM version from the server*/
int SCSCP_sc_readCIMVersion(SCSCP_Fileclient* client, SCSCP_status* status);
   /*! write the Connection information Message from the server*/
int SCSCP_sc_readCIM(SCSCP_Fileclient* client, const char **supportedversion,SCSCP_status* status);
   /*! write the CIM version to the server*/
int SCSCP_sc_writeCIMVersion(SCSCP_Fileclient* client, const char *supportedversion);

/*! set that the client receive the quit message */
void SCSCP_sc_setrecvquit(SCSCP_Fileclient* client);


int SCSCP_io_flush(SCSCP_io* stream, SCSCP_status* status);
int SCSCP_io_readxmldoc(SCSCP_io *stream, SCSCP_xmlnodeptr* iter, SCSCP_status *status);
int SCSCP_io_ReadPI(SCSCP_io* stream, SCSCP_msgtype* msgtype, char *bufferPI, SCSCP_status* status);
int SCSCP_io_writelen(SCSCP_io* stream, const void *buffer, size_t len, SCSCP_status* status);
int SCSCP_io_writef(SCSCP_io* stream, SCSCP_status* status, const char *buffer, ...);
int SCSCP_io_writeOMIsizet(SCSCP_io* stream, size_t value, SCSCP_status* status);
int SCSCP_io_writeOMEwithOMSOMSTR(SCSCP_io* stream, const char *cdname, const char*symbolname, const char *message, 
                                  SCSCP_status* status);
int SCSCP_io_writePairOMSOMIsizet(SCSCP_io* stream, const char *cdname, const char*symbolname, size_t value, 
                                  SCSCP_status* status);
int SCSCP_io_writePairOMSOMIint(SCSCP_io* stream, const char *cdname, const char*symbolname, int value, 
                                  SCSCP_status* status);
int SCSCP_io_writePairOMSOMSTR(SCSCP_io* stream, const char *cdname, const char*symbolname, const char* value, 
                                  SCSCP_status* status);
int SCSCP_io_writeSCSCPSTART(SCSCP_io* stream, SCSCP_status* status);
int SCSCP_io_writeSCSCPEND(SCSCP_io* stream, SCSCP_status* status);
int SCSCP_io_writeSCSCPCANCEL(SCSCP_io* stream, SCSCP_status* status);

#if defined (__cplusplus)
}
#endif /* (__cplusplus) */

#endif /*__SCSCPFILECLIENT_H__*/
