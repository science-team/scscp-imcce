/*-----------------------------------------------------------------*/
/*! 
  \file scscpremoteobject.cxx
  \brief SCSCP remote object functions
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009, 2012,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/
#if HAVE_STRING_H
#include <string.h>
#endif

#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpdebug.h"
#include "scscpfileserver.h"
#include "scscpfileclient.h"


/*-----------------------------------------------------------------*/
/*! store during a session a remote object and returns its reference name 
   @param client (inout) client session
   @param status (inout) error type
   @param callbackwriteargs (in) function to write arguments
   @param param (in) opaque object given to callbackwriteargs
   @param cookiename (out) name of the remote object
*/
/*-----------------------------------------------------------------*/
int SCSCP_sc_remoteobjectstoresessionhook(SCSCP_socketclient* client,  
                          int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status),
                          void *param, char ** cookiename, SCSCP_status* status)
{
 int res;
 SCSCP_xmlnodeptr node;
 const char *buf;
 SCSCP_debugprint("SCSCP_sc_remoteobjectstoresessionhook(%p, %p, %p, %p, %p)  - enter\n", client, callbackwriteargs, param,cookiename, status);
 
 res = SCSCP_sc_executehookxmlnode(client, SCSCP_option_return_cookie, "scscp2", "store_session",
                            callbackwriteargs, param, &node, status);
 if (res) 
 {
  res = SCSCP_xmlnode_readOMR(&node, &buf);
  if (res==0) SCSCP_status_seterror(status, SCSCP_STATUS_OPENMATHNOTVALID);
 }
 *cookiename = res?SCSCP_strdup(buf,status):NULL;

 SCSCP_debugprint("SCSCP_sc_remoteobjectstoresessionhook( %s, %d)  - return\n", *cookiename==NULL?"NULL":*cookiename, SCSCP_status_is(status));
 return res;
}
                          
/*-----------------------------------------------------------------*/
/*! store a remote object and returns its reference name 
    This object will useable for multiple session.
   @param client (inout) client session
   @param status (inout) error type
   @param callbackwriteargs (in) function to write arguments
   @param param (in) opaque object given to callbackwriteargs
   @param cookiename (out) name of the remote object
*/
/*-----------------------------------------------------------------*/
int SCSCP_sc_remoteobjectstorepersistenthook(SCSCP_socketclient* client,  
                          int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status),
                          void *param, char ** cookiename, SCSCP_status* status)
{
 int res;
 SCSCP_xmlnodeptr node;
 const char *buf;
 SCSCP_debugprint("SCSCP_sc_remoteobjectstoresessionhook(%p, %p, %p, %p, %p)  - enter\n", client, callbackwriteargs, param,cookiename, status);
 
 res = SCSCP_sc_executehookxmlnode(client, SCSCP_option_return_cookie, "scscp2", "store_persistent",
                            callbackwriteargs, param, &node, status);
 if (res) res = SCSCP_xmlnode_readOMR(&node, &buf);
 *cookiename = res?SCSCP_strdup(buf,status):NULL;

 SCSCP_debugprint("SCSCP_sc_remoteobjectstoresessionhook( %s, %d)  - return %d\n", *cookiename==NULL?"NULL":*cookiename, SCSCP_status_is(status), res);
 return res;
}

/*-----------------------------------------------------------------*/
/*! write the name of the cookie as an OpenMath string
   @param stream (inout) stream session
   @param status (inout) error type
   @param param (in) name of the remote object
*/
/*-----------------------------------------------------------------*/
static int SCSCP_sc_remoteobjecthookwrite(SCSCP_io* stream, void *param, SCSCP_status *status)
{
 const char *cookiename = (const char*)param;
 return SCSCP_io_writeOMR(stream, cookiename, status);
}

/*-----------------------------------------------------------------*/
/*! retrieve the value of a remote object and returns its reference name
   @param client (inout) client session
   @param status (inout) error type
   @param node (out) xml node 
   @param cookiename (in) name of the remote object
*/
/*-----------------------------------------------------------------*/
int SCSCP_sc_remoteobjectretrievexmlnode(SCSCP_socketclient* client, const char * cookiename, SCSCP_xmlnodeptr* node,SCSCP_status* status)
{
 int res;

 SCSCP_debugprint("SCSCP_sc_remoteobjectretrievexmlnode(%p, %s, %p, %p)  - enter\n", client, cookiename, node, status);
 
 res = SCSCP_sc_executehookxmlnode(client, SCSCP_option_return_object, "scscp2", "retrieve",
                           SCSCP_sc_remoteobjecthookwrite, (void *)cookiename , node, status);

 SCSCP_debugprint("SCSCP_sc_remoteobjectretrievexmlnode( %p, %d)  - returns %d\n", *node, SCSCP_status_is(status), res);
 return res;
}
                          
/*-----------------------------------------------------------------*/
/*! retrieve the value of a remote object and returns its reference name
   @param client (inout) client session
   @param status (inout) error type
   @param openmathbuffer (out) openmath string
   @param cookiename (in) name of the remote object
*/
/*-----------------------------------------------------------------*/
int SCSCP_sc_remoteobjectretrievestr(SCSCP_socketclient* client, const char * cookiename, char** openmathbuffer,SCSCP_status* status)
{
 int res;

 SCSCP_debugprint("SCSCP_sc_remoteobjectretrievestr(%p, %s, %p, %p)  - enter\n", client, cookiename, openmathbuffer, status);
 
 res = SCSCP_sc_executehookstr(client, SCSCP_option_return_object, "scscp2", "retrieve",
                           SCSCP_sc_remoteobjecthookwrite, (void *)cookiename , openmathbuffer, status);

 SCSCP_debugprint("SCSCP_sc_remoteobjectretrievestr( %p, %d)  - returns %d\n", *openmathbuffer, SCSCP_status_is(status), res);
 return res;
}

/*-----------------------------------------------------------------*/
/*! unbind a remote object
   @param client (inout) client session
   @param status (inout) error type
   @param cookiename (in) name of the remote object
*/
/*-----------------------------------------------------------------*/
int SCSCP_sc_remoteobjectunbind(SCSCP_socketclient* client, const char * cookiename, SCSCP_status* status)
{
 int res;
 SCSCP_xmlnodeptr node;
 
 SCSCP_debugprint("SCSCP_sc_remoteobjectunbind(%p, %s, %p)  - enter\n", client, cookiename, status);
 
 res = SCSCP_sc_executehookxmlnode(client, SCSCP_option_return_nothing, "scscp2", "unbind",
                           SCSCP_sc_remoteobjecthookwrite, (void*)cookiename, &node, status);

 SCSCP_debugprint("SCSCP_sc_remoteobjectunbind(%d)  - return %d\n",  SCSCP_status_is(status),res);
 return res;
}

