/*-----------------------------------------------------------------*/
/*! 
  \file scscp.h
  \brief SCSCP library - C interface.
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009,2010, 2011,  2012, 2013, 2014, 2015, 2016, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#ifndef __SCSCP_H__
#define __SCSCP_H__


/*----------------------------------------------------------------------------------------------*/
/* private part of the library */
/*----------------------------------------------------------------------------------------------*/
typedef struct SCSCP_stFileclient* SCSCP_socketclient;
typedef struct SCSCP_stFileserver* SCSCP_socketserver;
typedef struct SCSCP_stFileclient* SCSCP_io;
typedef struct SCSCP_stFileclient* SCSCP_incomingclient;
typedef struct SCSCP_optionsprocedurereturn* SCSCP_returnoptions;
typedef struct SCSCP_procedurecalloptions* SCSCP_calloptions;
typedef struct SCSCP_omnode_t* SCSCP_xmlnodeptr;
typedef struct SCSCP_omattr_t* SCSCP_xmlattrptr;

/*----------------------------------------------------------------------------------------------*/
/* windows specific support */
/*----------------------------------------------------------------------------------------------*/
/* Support for WINDOWS Dll:
   Check if we are inside a SCSCP build, and if so export the functions.
   Otherwise does the same thing as SCSCP */
#if defined (__GNUC__)
#define __SCSCP_DECLSPEC_EXPORT  __declspec(__dllexport__)
#define __SCSCP_DECLSPEC_IMPORT  __declspec(__dllimport__)
#endif
#if defined (_MSC_VER) || defined (__BORLANDC__)
#define __SCSCP_DECLSPEC_EXPORT  __declspec(dllexport)
#define __SCSCP_DECLSPEC_IMPORT  __declspec(dllimport)
#endif
#ifdef __WATCOMC__
#define __SCSCP_DECLSPEC_EXPORT  __export
#define __SCSCP_DECLSPEC_IMPORT  __import
#endif
#ifdef __IBMC__
#define __SCSCP_DECLSPEC_EXPORT  _Export
#define __SCSCP_DECLSPEC_IMPORT  _Import
#endif

#if defined (__SCSCP_LIBSCSCP_DLL)
#if __SCSCP_LIBSCSCP_DLL
/* compile as a dll */
#if defined(__SCSCP_WITHIN_SCSCP)
#define __SCSCP_DECLSPEC __SCSCP_DECLSPEC_EXPORT
#else
#define __SCSCP_DECLSPEC __SCSCP_DECLSPEC_IMPORT
#endif
#endif
#endif /*__SCSCP_DECLSPEC*/

#if !defined(__SCSCP_DECLSPEC)
/* other cases */
#define __SCSCP_DECLSPEC 
#endif /*__SCSCP_DECLSPEC*/


/*----------------------------------------------------------------------------------------------*/
/* public part */
/*----------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif /*defined (__cplusplus)*/

/*-----------------------------------------------------------------*/
/* types */
/*-----------------------------------------------------------------*/

/*! string type */
typedef char* SCSCP_string;

/*! const string type */
typedef const char* const_SCSCP_string;

/*! time limits type */
typedef size_t SCSCP_time;

/*! memory limits type */
typedef size_t SCSCP_memory;


/*! status type */
typedef struct
{
    int   m_code;        /*!< code value */
    /*extended attributes  : only for PROCEDURETERMINATED */
    char *m_cdname;      /*!< name of the content dictionnary */
    char *m_symbolname;  /*!< name of the symbol */
    char *m_message;     /*!< message content */
    char *m_fullformattedmessage; /*! full formatted message for strerror */
} SCSCP_status;

/*-----------------------------------------------------------------*/
/*! enumeration of return type */
/*-----------------------------------------------------------------*/
typedef enum
{
    SCSCP_option_return_object, /*!< return the object*/
    SCSCP_option_return_cookie, /*!< return a cookie*/
    SCSCP_option_return_nothing /*!< return nothing*/
} SCSCP_option_return;

/*-----------------------------------------------------------------*/
/*! enumeration of message type*/
/*-----------------------------------------------------------------*/
typedef enum
{
    SCSCP_msgtype_ProcedureTerminated, /*!< message received is procedure terminated */
    SCSCP_msgtype_ProcedureCompleted,  /*!< message received is procedure completed */
    SCSCP_msgtype_ProcedureCall,       /*!< message received is procedure call */
    SCSCP_msgtype_Interrupt            /*!< message received is interrupt */
} SCSCP_msgtype;

/*-----------------------------------------------------------------*/
/*! enumeration of encoding type*/
/*-----------------------------------------------------------------*/
typedef enum
{
    SCSCP_encodingtype_XML,            /*!< OpenMath objects are written using XML encoding */
    SCSCP_encodingtype_Binary          /*!< OpenMath objects are written using Binary encoding */
} SCSCP_encodingtype;

/*-----------------------------------------------------------------*/
/*! enumeration of OpenMath nodes */
/*-----------------------------------------------------------------*/
typedef enum
{
    SCSCP_omtype_CONTENT,      /*!< content node only */
    SCSCP_omtype_OMI,          /*!< OMI */
    SCSCP_omtype_OMF,          /*!< OMF  */
    SCSCP_omtype_OMV,          /*!< OMVAR */
    SCSCP_omtype_OMS,          /*!< OMS */
    SCSCP_omtype_OMSTR,        /*!< OMSTR */
    SCSCP_omtype_OMB,          /*!< OMB */
    SCSCP_omtype_OMFOREIGN,    /*!< OMFOREIGN */
    SCSCP_omtype_OMA,          /*!< OMA */
    SCSCP_omtype_OMBIND,       /*!< OMBIND */
    SCSCP_omtype_OMATTR,       /*!< OMATTR */
    SCSCP_omtype_OME,          /*!< OME */
    SCSCP_omtype_OMATP,        /*!< OMATP */
    SCSCP_omtype_OMOBJ,        /*!< OMOBJ */
    SCSCP_omtype_OMBVAR,       /*!< OMBVAR */
    SCSCP_omtype_OMR           /*!< OMR external */
} SCSCP_omtype;

/*----------------------------------------------------------------------------------------------*/
/* definition of the SCSCP library version */
/*! version : major number of SCSCP library */
#define SCSCP_VERSION_MAJOR 1
/*! version : minor number of SCSCP library */
#define SCSCP_VERSION_MINOR 0
/*! version : patch number of SCSCP library */
#define SCSCP_VERSION_PATCH 3 
    
/*! return the SCSCP library version as string */
__SCSCP_DECLSPEC const char* SCSCP_get_libversionstr();
/*! return the SCSCP library version as numbers */
__SCSCP_DECLSPEC void SCSCP_get_libversionnum(unsigned int *major, unsigned int *minor, unsigned int *patch);

/*! check the SCSCP library compatibility with value of the header */
#define SCSCP_CHECK_LIBVERSION() SCSCP_check_libversion(SCSCP_VERSION_MAJOR, SCSCP_VERSION_MINOR, SCSCP_VERSION_PATCH)

/*----------------------------------------------------------------------------------------------*/
/* definition of the standard version number of  SCSCP  */
/*! definition of the standard version number of  SCSCP  : version 1.2 */
#define SCSCP_PROTOCOL_VERSION_1_2 "1.2"
/*! definition of the standard version number of  SCSCP  : version 1.3 */
#define SCSCP_PROTOCOL_VERSION_1_3 "1.3"
/* for developers, if we add a new version, remember to update SCSCP_co_readoptions */

/*----------------------------------------------------------------------------------------------*/
/*! default value on which port should listen the SCSCP server */
#define SCSCP_PROTOCOL_DEFAULTPORT 26133

/*----------------------------------------------------------------------------------------------*/
/* functions for the SCSCP client and server  */
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/* defines and functions for status object */
/*----------------------------------------------------------------------------------------------*/
/*!possible value for SCSCP_status */
/*! SCSCP library error code : no error */
#define SCSCP_STATUS_OK 0
/*! SCSCP library error code : The object client passed to the function is NULL */
#define SCSCP_STATUS_CLIENTOBJECTNULL (-1)
/*! SCSCP library error code : Not enough memory */
#define SCSCP_STATUS_NOMEM (-2)
/*!  errno is set to a system error */
#define SCSCP_STATUS_ERRNO (-3)
/*! SCSCP library error code : The call options passed to the function is NULL */
#define SCSCP_STATUS_CALLOPTIONSOBJECTNULL (-4)
/*! SCSCP library error code : The interrupt message '<?scscp cancel ?> was received */
#define SCSCP_STATUS_RECVCANCEL (-8)
/*! SCSCP library error code : The interrupt message '<?scscp quit ?> was received */
#define SCSCP_STATUS_RECVQUIT (-9)
/*! SCSCP library error code : The return options passed to the function is NULL */
#define SCSCP_STATUS_RETURNOPTIONSOBJECTNULL (-5)
/*! SCSCP library error code : The object server passed to the function is NULL */
#define SCSCP_STATUS_SERVEROBJECTNULL (-6)
/*! SCSCP library error code : The object stream passed to the function is NULL */
#define SCSCP_STATUS_STREAMOBJECTNULL (-7)
/*! SCSCP library error code : The call identifier isn't defined in the options */
#define SCSCP_STATUS_CALLIDISNOTSET (-10) 
/*! SCSCP library error code : The return type isn't defined in the options */
#define SCSCP_STATUS_RETURNTYPEISNOTSET (-11) 
/*! SCSCP library error code : The runtime usage isn't available in the procedure return message */
#define SCSCP_STATUS_USAGEUNKNOWNRUNTIME (-20)
/*! SCSCP library error code : The memory usage isn't available in the procedure return message */
#define SCSCP_STATUS_USAGEUNKNOWNMEM (-21)
/*! SCSCP library error code : The information message isn't available in the procedure return message */
#define SCSCP_STATUS_USAGEUNKNOWNMESSAGE (-27)
/*! SCSCP library error code : The runtime limit usage isn't available in the procedure call message */
#define SCSCP_STATUS_USAGEUNKNOWNRUNTIMELIMIT (-22)
/*! SCSCP library error code : The min memory isn't available in the procedure call message */
#define SCSCP_STATUS_USAGEUNKNOWNMINMEMORY (-23)
/*! SCSCP library error code : The max memory isn't available in the procedure call message */
#define SCSCP_STATUS_USAGEUNKNOWNMAXMEMORY (-24)
/*! SCSCP library error code : The debug level isn't available in the procedure call message */
#define SCSCP_STATUS_USAGEUNKNOWNDEBUGLEVEL (-25)
/*! SCSCP library error code : The return type isn't available in the procedure call message */
#define SCSCP_STATUS_USAGEUNKNOWNRETURNTYPE (-26)
/*! SCSCP library error code : The version negotiation fails */
#define SCSCP_STATUS_VERSIONNEGOTIATIONFAILED (-30)
/*! SCSCP library error code : The remote execution fails */
#define SCSCP_STATUS_EXECFAILED (-31)
/*! SCSCP library error code : The openmath expression isn't valid */
#define SCSCP_STATUS_OPENMATHNOTVALID (-32)

/*! default status value to initialize the status to SCSCP_STATUS_OK */
#define SCSCP_STATUS_INITIALIZER SCSCP_status_getinitializer()

/*! default status value to ignore returned value */
#define SCSCP_STATUS_IGNORE NULL



/*! return the numerical value of the status (if NULL returns always OK) */
#define SCSCP_status_is(status) ((status)==NULL?SCSCP_STATUS_OK:(status)->m_code)
/*! return a pointer to the corresponding message string */
__SCSCP_DECLSPEC const char* SCSCP_status_strerror(const SCSCP_status* status);
/*! clear the status object */
__SCSCP_DECLSPEC void SCSCP_status_clear(SCSCP_status* src);

/*! copy the content of src to dst */
__SCSCP_DECLSPEC void SCSCP_status_copy(SCSCP_status* dst, const SCSCP_status *src);
/*! return 0 if status indicates an error */
__SCSCP_DECLSPEC int SCSCP_status_isok(const SCSCP_status* status);

/*----------------------------------------------------------------------------------------------*/
/* defines and functions for procedure call options object */
/*----------------------------------------------------------------------------------------------*/
/*! default procedure call option to send to server */
#define SCSCP_CALLOPTIONS_DEFAULT NULL

/*! initialize the call options and set status if an error occurs */
__SCSCP_DECLSPEC int SCSCP_co_init(SCSCP_calloptions* options, SCSCP_status* status);
/*! clear the call options previously initialized by SCSCP_co_init */
__SCSCP_DECLSPEC int SCSCP_co_clear(SCSCP_calloptions* options, SCSCP_status* status);
/*! set the procedure call ID (buffer can't be released before calling SCSCP_co_clear) */
__SCSCP_DECLSPEC int SCSCP_co_set_callid(SCSCP_calloptions* options, const char *buffer, SCSCP_status* status);
/*! get the procedure call ID */
__SCSCP_DECLSPEC int SCSCP_co_get_callid(SCSCP_calloptions* options, const char **buffer, SCSCP_status* status);
/*! set the runtime limit */
__SCSCP_DECLSPEC int SCSCP_co_set_runtimelimit(SCSCP_calloptions* options, size_t p_time, SCSCP_status* status);
/*! get the runtime limit */
__SCSCP_DECLSPEC int SCSCP_co_get_runtimelimit(SCSCP_calloptions* options, size_t* p_time, SCSCP_status* status);
/*! set the minimum memory */
__SCSCP_DECLSPEC int SCSCP_co_set_minmemory(SCSCP_calloptions* options, size_t memsize, SCSCP_status* status);
/*! get the minimum memory */
__SCSCP_DECLSPEC int SCSCP_co_get_minmemory(SCSCP_calloptions* options, size_t* memsize, SCSCP_status* status);
/*! set the maximum memory */
__SCSCP_DECLSPEC int SCSCP_co_set_maxmemory(SCSCP_calloptions* options, size_t memsize, SCSCP_status* status);
/*! get the maximum memory */
__SCSCP_DECLSPEC int SCSCP_co_get_maxmemory(SCSCP_calloptions* options, size_t* memsize, SCSCP_status* status);
/*! set the debug level */
__SCSCP_DECLSPEC int SCSCP_co_set_debuglevel(SCSCP_calloptions* options, int debuglevel, SCSCP_status* status);
/*! get the debug level */
__SCSCP_DECLSPEC int SCSCP_co_get_debuglevel(SCSCP_calloptions* options, int* debuglevel, SCSCP_status* status);
/*! set the return type */
__SCSCP_DECLSPEC int SCSCP_co_set_returntype(SCSCP_calloptions* options, SCSCP_option_return returntype, SCSCP_status* status);
/*! get the return type */
__SCSCP_DECLSPEC int SCSCP_co_get_returntype(SCSCP_calloptions* options, SCSCP_option_return* returntype, SCSCP_status* status);
/*! return the encoding type */
__SCSCP_DECLSPEC int SCSCP_co_get_encodingtype(SCSCP_calloptions* options, SCSCP_encodingtype* encodingtype, SCSCP_status* status);

/*----------------------------------------------------------------------------------------------*/
/* defines and functions for procedure return options object */
/*----------------------------------------------------------------------------------------------*/
/*! default procedure return option to ignore returned value  */
#define SCSCP_RETURNOPTIONS_IGNORE NULL

/*! initialize the return options and set status if an error occurs */
__SCSCP_DECLSPEC int SCSCP_ro_init(SCSCP_returnoptions* options, SCSCP_status* status);
/*! clear the return options previously initialized by SCSCP_ro_init */
__SCSCP_DECLSPEC int SCSCP_ro_clear(SCSCP_returnoptions* options, SCSCP_status* status);
/*! set the procedure call ID */
__SCSCP_DECLSPEC int SCSCP_ro_set_callid(SCSCP_returnoptions* options, const char *buffer, SCSCP_status* status);
/*! get the procedure call ID */
__SCSCP_DECLSPEC int SCSCP_ro_get_callid(SCSCP_returnoptions* options, const char **buffer, SCSCP_status* status);
/*! set the runtime usage */
__SCSCP_DECLSPEC int SCSCP_ro_set_runtime(SCSCP_returnoptions* options, size_t p_time, SCSCP_status* status);
/*! get the runtime usage */
__SCSCP_DECLSPEC int SCSCP_ro_get_runtime(SCSCP_returnoptions* options, size_t* p_time, SCSCP_status* status);
/*! set the memory usage */
__SCSCP_DECLSPEC int SCSCP_ro_set_memory(SCSCP_returnoptions* options, size_t mem, SCSCP_status* status);
/*! get the memory usage */
__SCSCP_DECLSPEC int SCSCP_ro_get_memory(SCSCP_returnoptions* options, size_t* mem, SCSCP_status* status);
/*! set the information message */
__SCSCP_DECLSPEC int SCSCP_ro_set_message(SCSCP_returnoptions* options, const char *buffer, SCSCP_status* status);
/*! get the information message */
__SCSCP_DECLSPEC int SCSCP_ro_get_message(SCSCP_returnoptions* options, const char **buffer, SCSCP_status* status);

/*----------------------------------------------------------------------------------------------*/
/* functions for the SCSCP_xmlnodeptr/SCSCP_xmlattr only  */
/*----------------------------------------------------------------------------------------------*/
/*! return the next node after the current xml node  */
__SCSCP_DECLSPEC SCSCP_xmlnodeptr SCSCP_xmlnode_getnext(SCSCP_xmlnodeptr curnode);
/*! return the name of the current xml node  */
__SCSCP_DECLSPEC const char* SCSCP_xmlnode_getname(SCSCP_xmlnodeptr curnode);
/*! return the first child node in the current xml node  */
__SCSCP_DECLSPEC SCSCP_xmlnodeptr SCSCP_xmlnode_getchild(SCSCP_xmlnodeptr curnode);
/*! read the content of the current node */
__SCSCP_DECLSPEC const char * SCSCP_xmlnode_getcontent(SCSCP_xmlnodeptr node);
/*! read the id of the current node */
__SCSCP_DECLSPEC const char * SCSCP_xmlnode_getid(SCSCP_xmlnodeptr node);
/*! read the first attribute of the current node  */
__SCSCP_DECLSPEC SCSCP_xmlattrptr SCSCP_xmlnode_getattr(SCSCP_xmlnodeptr curnode);

/*! return the next attribute after attr */
__SCSCP_DECLSPEC SCSCP_xmlattrptr SCSCP_xmlattr_getnext(SCSCP_xmlattrptr attr);
/*! get the content of the attribute */
__SCSCP_DECLSPEC int SCSCP_xmlattr_getvalue(SCSCP_xmlattrptr attr, const char **name, const char **value);

/*! read an OpenMath Symbol */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMS(SCSCP_xmlnodeptr* curnode, const char **cdname, const char **symbolname);
/*! read an OpenMath integer */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMIstr(SCSCP_xmlnodeptr* curnode, const char **value);
/*! read an OpenMath integer */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMIint(SCSCP_xmlnodeptr* curnode, int *value);
/*! read an OpenMath floating-point */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMFstr(SCSCP_xmlnodeptr* curnode, const char **value, int *base);
/*! read an OpenMath floating-point */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMFdouble(SCSCP_xmlnodeptr* curnode, double *value);
/*! read an OpenMath string */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMSTR(SCSCP_xmlnodeptr* curnode, const char **value);
/*! read an OpenMath reference */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMR(SCSCP_xmlnodeptr* curnode, const char **value);
/*! read an OpenMath variable */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMV(SCSCP_xmlnodeptr* curnode, const char **value);
/*! read an OpenMath byte arra */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readOMB(SCSCP_xmlnodeptr* curnode, const void **buffer, size_t* lenbuffer);
/*! read an OpenMath Symbol , check the symbol name with symbolname and the CD anme and return the next OpenMath string  */
__SCSCP_DECLSPEC int SCSCP_xmlnode_readpairOMSOMSTR(SCSCP_xmlnodeptr* curnode, const char *cdname, const char *symbolname, const char **buffer);


/*----------------------------------------------------------------------------------------------*/
/* functions for the SCSCP low I/O   */
/*----------------------------------------------------------------------------------------------*/
/* These function must be called only from function SCSCP_xx_xxxcallback ! */
/*! write the openmath expression directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_write(SCSCP_io* stream, const char *buffer, SCSCP_status* status);
/*! write buffer as an OpenMath string <OMSTR></OMSTR> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMSTR(SCSCP_io* stream, const char *buffer, const char *id, SCSCP_status* status);
/*! write x as an OpenMath float-point <OMF></OMF> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMFdouble(SCSCP_io* stream, double x, const char *id, SCSCP_status* status);
/*! write buffer as an OpenMath floating-point <OMF></OMF> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMFstr(SCSCP_io* stream, const char *buffer, const char *id, SCSCP_status* status);
/*! write x as an OpenMath integer <OMI></OMI> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMIint(SCSCP_io* stream, int x, const char *id, SCSCP_status* status);
/*! write x as an OpenMath integer <OMI></OMI> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMIlonglong(SCSCP_io* stream, long long x, const char *id, SCSCP_status* status);
/*! write buffer as an OpenMath integer <OMI></OMI> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMIstr(SCSCP_io* stream, const char* buffer, const char *id, SCSCP_status* status);
/*! write symbolname/cdname  as an OpenMath symbol <OMS cd="..." name="..."> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMS(SCSCP_io* stream, const char* cdname, const char* symbolname, const char *id, SCSCP_status* status);
/*! write buffer  as an OpenMath variable <OMV></OMV> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMV(SCSCP_io* stream, const char *buffer, const char *id,  SCSCP_status* status);
/*! write buffer  as an OpenMath reference <OMR href="..."/> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMR(SCSCP_io* stream, const char *buffer, SCSCP_status* status);
/*! write buffer  as an OpenMath byte array <OMB>...</OMB> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeOMB(SCSCP_io* stream, const void *buffer, size_t lenbuffer, const char *id, SCSCP_status* status);

/* structured openmath object */
/*! write the beginning OpenMath tag <OMA> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writebeginOMA(SCSCP_io* stream, const char *id,  SCSCP_status* status);
/*! write the end OpenMath tag </OMA> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeendOMA(SCSCP_io* stream, SCSCP_status* status);
/*! write the beginning OpenMath tag <OMATP> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writebeginOMATP(SCSCP_io* stream, const char *id,  SCSCP_status* status);
/*! write the end OpenMath tag </OMATP> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeendOMATP(SCSCP_io* stream, SCSCP_status* status);
/*! write the beginning OpenMath tag <OMATTR> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writebeginOMATTR(SCSCP_io* stream, const char *id,  SCSCP_status* status);
/*! write the end OpenMath tag </OMATTR> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeendOMATTR(SCSCP_io* stream, SCSCP_status* status);
/*! write the beginning OpenMath tag <OME> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writebeginOME(SCSCP_io* stream, const char *id,  SCSCP_status* status);
/*! write the end OpenMath tag </OME> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeendOME(SCSCP_io* stream, SCSCP_status* status);
/*! write the beginning OpenMath tag <OMOBJ> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writebeginOMOBJ(SCSCP_io* stream, SCSCP_status* status);
/*! write the end OpenMath tag </OMOBJ> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeendOMOBJ(SCSCP_io* stream, SCSCP_status* status);
/*! write the beginning OpenMath tag <OMBIND> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writebeginOMBIND(SCSCP_io* stream, const char *id, SCSCP_status* status);
/*! write the end OpenMath tag </OMBIND> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeendOMBIND(SCSCP_io* stream, SCSCP_status* status);
/*! write the beginning OpenMath tag <OMFOREIGN> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writebeginOMFOREIGN(SCSCP_io* stream, const char *id, SCSCP_status* status);
/*! write the end OpenMath tag </OMFOREIGN> directly to the SCSCP client or server */
__SCSCP_DECLSPEC int SCSCP_io_writeendOMFOREIGN(SCSCP_io* stream, SCSCP_status* status);


/*----------------------------------------------------------------------------------------------*/
/* functions for the SCSCP client only  */
/*----------------------------------------------------------------------------------------------*/
/*! initialize the object client and set status if an error occurs */
__SCSCP_DECLSPEC int SCSCP_sc_init(SCSCP_socketclient* client, SCSCP_status* status, ...); 
/*! clear the object client previously initialized by SCSCP_sc_init */
__SCSCP_DECLSPEC int SCSCP_sc_clear(SCSCP_socketclient* client, SCSCP_status* status); 

/*! connect the client to a server on the specified host and port */
__SCSCP_DECLSPEC int SCSCP_sc_connect(SCSCP_socketclient* client, const char *machine, int port, SCSCP_status* status); 
/*! close the connection of the client to a server previously opened by SCSCP_sc_connect */
__SCSCP_DECLSPEC int SCSCP_sc_close(SCSCP_socketclient* client, SCSCP_status* status); 

/*! set the current encoding for the OpenMath objects */
__SCSCP_DECLSPEC int SCSCP_sc_set_encodingtype(SCSCP_socketclient* client, SCSCP_encodingtype encodingtype, SCSCP_status* status);
/*! get the current encoding for the OpenMath objects */
__SCSCP_DECLSPEC int SCSCP_sc_get_encodingtype(SCSCP_socketclient* client, SCSCP_encodingtype* encodingtype, SCSCP_status* status);


/*! send a procedure call with a openmath expression as arguments to the SCSCP server */
__SCSCP_DECLSPEC int SCSCP_sc_callsendstr(SCSCP_socketclient* client, SCSCP_calloptions* options, 
                          const char *openmathbuffer, SCSCP_status* status);
/*! send a procedure call to the SCSCP server. The arguments  are written by callbackwriteargs */
__SCSCP_DECLSPEC int SCSCP_sc_callsendhook(SCSCP_socketclient* client, SCSCP_calloptions* options, 
                          int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status),
                          void *param, SCSCP_status* status);
/*! read the attribute of the answer for the procedure call and the type of the answer message */
__SCSCP_DECLSPEC int SCSCP_sc_callrecvheader(SCSCP_socketclient* client, SCSCP_returnoptions* options,
                            SCSCP_msgtype *msgtype, SCSCP_status* status);

/* for "interrupt" message */
/*! send the "interrupt" message to the server */
__SCSCP_DECLSPEC int SCSCP_sc_callsendinterrupt (SCSCP_socketclient* client, const char* call_id, SCSCP_status* status); 
                            

/* for "procedure terminated" message */
/*! read the content of a "procedure terminated message" 
    and the error message is stored in  errortype and messagebuffer  */
__SCSCP_DECLSPEC int SCSCP_sc_callrecvterminated(SCSCP_socketclient* client, char **cdname, char **symbolname, char **messagebuffer, SCSCP_status* status); 

/* for "procedure completed" message */
/*! read the content of a "procedure completed message" and store it in openmathbuffer */
__SCSCP_DECLSPEC int SCSCP_sc_callrecvcompleted(SCSCP_socketclient* client, char **openmathbuffer, SCSCP_status* status); 

/*! read the answer of the procedure call and store it in a string buffer */
__SCSCP_DECLSPEC int SCSCP_sc_callrecvstr(SCSCP_socketclient* client, SCSCP_returnoptions* options,
                         SCSCP_msgtype *msgtype, char **openmathbuffer, SCSCP_status* status); 

/*! return the first node in the current location of the openmath message */
__SCSCP_DECLSPEC SCSCP_xmlnodeptr SCSCP_sc_getxmlnode(SCSCP_socketclient *client, SCSCP_status* status);

/*! return node and its children as a raw string */
__SCSCP_DECLSPEC char* SCSCP_sc_getxmlnoderawstring(SCSCP_socketclient* client, SCSCP_xmlnodeptr node, SCSCP_status *status);

/* for "information" message */
/*!  send the "information" message to the server. */
__SCSCP_DECLSPEC int SCSCP_sc_infomessagesend (SCSCP_socketclient* client, const char * messagebuffer, SCSCP_status* status); 

/* high-level functions */
/* send a procedure call to the server with symbolname and cdname and wait for the answer */
__SCSCP_DECLSPEC int SCSCP_sc_executehookxmlnode(SCSCP_socketclient* client, SCSCP_option_return returntype, const char *cdname, const char*symbolname,
                              int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status), void *paramwrite, 
                              SCSCP_xmlnodeptr* node,
                              SCSCP_status* status);
/* send a procedure call to the server with symbolname and cdname and wait for the answer */
__SCSCP_DECLSPEC int SCSCP_sc_executehookstr(SCSCP_socketclient* client, SCSCP_option_return returntype, const char *cdname, const char*symbolname,
                              int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status), void *paramwrite, 
                              char** openmathbuffer,
                              SCSCP_status* status);



/*----------------------------------------------------------------------------------------------*/
/* functions for remote objects  */
/*----------------------------------------------------------------------------------------------*/
/*!  store during a session a remote object and returns its reference name */
__SCSCP_DECLSPEC int SCSCP_sc_remoteobjectstoresessionhook(SCSCP_socketclient* client,  
                          int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status),
                          void *param, char ** cookiename, SCSCP_status* status); 
/*!  store for multiple sessions a remote object and returns its reference name */
__SCSCP_DECLSPEC int SCSCP_sc_remoteobjectstorepersistenthook(SCSCP_socketclient* client,  
                          int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status),
                          void *param, char ** cookiename, SCSCP_status* status); 
                          
/*!  retrieve the value of a remote object and returns a pointer to the XML tree of this object */
__SCSCP_DECLSPEC int SCSCP_sc_remoteobjectretrievexmlnode(SCSCP_socketclient* client, const char * cookiename, SCSCP_xmlnodeptr* node,SCSCP_status* status); 
/*!  retrieve the value of a remote object and returns a string  */
__SCSCP_DECLSPEC int SCSCP_sc_remoteobjectretrievestr(SCSCP_socketclient* client, const char * cookiename, char **openmathbuffer,SCSCP_status* status); 
                          
/*!  unbind a remote object  */
__SCSCP_DECLSPEC int SCSCP_sc_remoteobjectunbind(SCSCP_socketclient* client, const char * cookiename, SCSCP_status* status); 


/*----------------------------------------------------------------------------------------------*/
/* functions for the SCSCP server only  */
/*----------------------------------------------------------------------------------------------*/
/*! initialize the object server and set status if an error occurs */
__SCSCP_DECLSPEC int SCSCP_ss_init(SCSCP_socketserver* server, SCSCP_status* status, const char *servicename, 
                  const char *serviceversion, const char *serviceid, ...); 
/*! clear the object server previously initialized by SCSCP_ss_init */
__SCSCP_DECLSPEC int SCSCP_ss_clear(SCSCP_socketserver* server, SCSCP_status* status); 

/*! the object server listen on the specified port */
__SCSCP_DECLSPEC int SCSCP_ss_listen(SCSCP_socketserver* server, int port, int firstavailable, SCSCP_status* status); 
/*! return the port from the server is listening */
__SCSCP_DECLSPEC int SCSCP_ss_getport(SCSCP_socketserver* server); 
/*! the object server terminates to listen */
__SCSCP_DECLSPEC int SCSCP_ss_close(SCSCP_socketserver* server, SCSCP_status* status);  

/*! the object accepts an incoming connection and returns a incoming client connection */
__SCSCP_DECLSPEC int SCSCP_ss_acceptclient(SCSCP_socketserver* server, SCSCP_incomingclient* incomingclient, SCSCP_status* status); 

/*----------------------------------------------------------------------------------------------*/
/* functions for the SCSCP incoming connection only (server only)  */
/*----------------------------------------------------------------------------------------------*/
/*! receive the "procedure call" message from the client :
  read the attribute of the incoming procedure call and the type of the incoming message 
  store the arguments in the openmathbuffer */
__SCSCP_DECLSPEC int SCSCP_ss_callrecvstr(SCSCP_incomingclient* incomingclient, SCSCP_calloptions* options,
                         SCSCP_msgtype *msgtype, char **openmathbuffer, SCSCP_status* status); 
/*! receive the "procedure call" message  : 
    read the attribute of the incoming procedure call and the type of the incoming message */
__SCSCP_DECLSPEC int SCSCP_ss_callrecvheader(SCSCP_incomingclient* incomingclient, SCSCP_calloptions* options,
                            SCSCP_msgtype *msgtype, SCSCP_status* status);

/*! the server sends the "procedure terminated" message with an error type and a text message to the client */
__SCSCP_DECLSPEC int SCSCP_ss_sendterminatedstr(SCSCP_incomingclient* incomingclient, SCSCP_returnoptions* options, 
                                const char *cdname, const char *symbolname,
                                const char *messagebuffer, SCSCP_status* status);

/*! the server sends the "procedure completed" message  with a openmath expression as arguments  to the client */
__SCSCP_DECLSPEC int SCSCP_ss_sendcompletedstr(SCSCP_incomingclient* incomingclient, SCSCP_returnoptions* options, 
                              const char *openmathbuffer, SCSCP_status* status); 

/*! the server sends the "procedure completed" message to the client. The arguments  are written by callbackwriteargs */
__SCSCP_DECLSPEC int SCSCP_ss_sendcompletedhook(SCSCP_incomingclient* incomingclient, SCSCP_returnoptions* options, 
                          int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status),
                          void *param, SCSCP_status* status);

/*!  the server sends the "information" message to the client. */
__SCSCP_DECLSPEC int SCSCP_ss_infomessagesend (SCSCP_incomingclient* client, const char * messagebuffer, SCSCP_status* status); 

/*! set the current encoding for the OpenMath objects */
__SCSCP_DECLSPEC int SCSCP_ss_set_encodingtype(SCSCP_incomingclient* client, SCSCP_encodingtype encodingtype, SCSCP_status* status);
/*! get the current encoding for the OpenMath objects */
__SCSCP_DECLSPEC int SCSCP_ss_get_encodingtype(SCSCP_incomingclient* client, SCSCP_encodingtype* encodingtype, SCSCP_status* status);

/*! terminate an incoming connection */
__SCSCP_DECLSPEC int SCSCP_ss_closeincoming(SCSCP_incomingclient* client, SCSCP_status* status);  

/*! return the first node in the current location of the openmath message */
__SCSCP_DECLSPEC SCSCP_xmlnodeptr SCSCP_ss_getxmlnode(SCSCP_incomingclient* client, SCSCP_status* status);

/*! return node and its children as a raw string */
__SCSCP_DECLSPEC char* SCSCP_ss_getxmlnoderawstring(SCSCP_incomingclient* client, SCSCP_xmlnodeptr node, SCSCP_status *status);



/*----------------------------------------------------------------------------------------------*/
/* private part of the library : required visible from outside  */
/*----------------------------------------------------------------------------------------------*/
/*! return initializer variable : this function is for internal use only */
__SCSCP_DECLSPEC SCSCP_status SCSCP_status_getinitializer(void);
/*! check the SCSCP library compatibility */
__SCSCP_DECLSPEC int SCSCP_check_libversion(unsigned int major, unsigned int minor, unsigned int patch);

#if defined (__cplusplus)
}
#endif /*defined (__cplusplus)*/

#endif /*__SCSCP_H__*/
