/*-----------------------------------------------------------------*/
/*! 
  \file scscpprocedurecompleted.cxx
  \brief SCSCP message : procedure completed.
   class  SCSCP_procedurecompleted
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2012,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/
#include <stdio.h>
#if HAVE_STRING_H
#include <string.h>
#endif
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpdebug.h"
#include "scscptags.h"
#include "scscpfileclient.h"
#include "scscpfileserver.h"
#include "scscpprocedurecompleted.h"


/*-----------------------------------------------------------------*/
/*! start to send the "procedure completed" message to the client. 
 @param client (inout) scscp client
 @param options (in) options of the call
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_procedurecompleted_write_start(SCSCP_io* stream, SCSCP_procedurecompletedoptions* options, 
                                   SCSCP_status* status)
{
 int res;
 
 SCSCP_debugprint("SCSCP_procedurecompleted_write_start(%p,%p, %p) - enter\n", stream, options, status);

 res = SCSCP_io_writeSCSCPSTART(stream, status);
 res &= SCSCP_io_writebeginOMOBJ(stream, status);
 	res &= SCSCP_io_writebeginOMATTR(stream, NULL, status);
 
        /*write the options*/
        res &= SCSCP_ro_write(stream, options, status);
 
 		res &= SCSCP_io_writebeginOMA(stream, NULL, status);
 
            /*write procedure name */ 
 			res &= SCSCP_io_writeOMS(stream, SCSCP_TAGS_cdname, SCSCP_TAGS_procedure_completed,NULL,  status);
 			
 SCSCP_debugprint("SCSCP_procedurecompleted_write_start(,%d) - returns %d\n", SCSCP_status_is(status), res);
 return res;
}


/*-----------------------------------------------------------------*/
/*! finish to send the "procedure completed" message to the client. 
 @param client (inout) scscp client
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_procedurecompleted_write_finish(SCSCP_io* stream, SCSCP_status* status)
{
 int res;
 
 SCSCP_debugprint("SCSCP_procedurecompleted_write_finish(%p, %p) - enter\n", stream,  status);


 		res = SCSCP_io_writeendOMA(stream, status); 
 	res &= SCSCP_io_writeendOMATTR(stream, status);
 res &= SCSCP_io_writeendOMOBJ(stream, status);
 res &= SCSCP_io_writeSCSCPEND(stream, status);
 
 SCSCP_debugprint("SCSCP_procedurecompleted_write_finish(,%d) - returns %d\n", SCSCP_status_is(status), res);
 return res;
}


/*-----------------------------------------------------------------*/
/*! send the "procedure completed" message to the client. 
    The arguments  are written by callbackwriteargs
 @param client (inout) scscp client
 @param options (in) options of the call
 @param callbackwriteargs (in) callback function 
             to write the parameters of the procedure call
 @param param (inout) opaque data given to callbackwriteargs
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_procedurecompleted_write(SCSCP_io* stream, SCSCP_procedurecompletedoptions* options, 
                          int (*callbackwriteargs)(SCSCP_io* stream, void *param, SCSCP_status* status),
                          void *param, SCSCP_status* status)
{
 int res;
 
 SCSCP_debugprint("SCSCP_procedurecompleted_write(%p,%p,%p, %p, %p) - enter\n", stream, options, callbackwriteargs,param, status);

 res = SCSCP_procedurecompleted_write_start(stream, options, status);
         /*write returned object */
 if (res) res = callbackwriteargs(stream, param, status);
 
 if (res) res = SCSCP_procedurecompleted_write_finish(stream, status);
 
 SCSCP_debugprint("SCSCP_procedurecompleted_write(,%d) - returns %d\n", SCSCP_status_is(status), res);
 return res;
}

/*-----------------------------------------------------------------*/
/*! read the content of a "procedure completed message" 
    and store it in openmathbuffer 
 @param client (inout) scscp client
 @param openmathbuffer (out) openmath buffer. 
                 This buffer must be freed with 'free'
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_sc_callrecvcompleted(SCSCP_socketclient* client, char **openmathbuffer,
                               SCSCP_status* status)
{
 int res = SCSCP_sc_checkNULL(client, status);
 if (res)
 {
   *openmathbuffer = SCSCP_sc_getxmlnoderawstring(client, SCSCP_sc_getxmlnode(client, status), status);
 }
 return res;
}


