/*-----------------------------------------------------------------*/
/*! 
  \file scscpcallid.c
  \brief SCSCP call ID implementation
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

   \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
This file is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute this file under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.

*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <stdio.h>
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpfileclient.h"
#include "scscpfileserver.h"
#include "scscpcallid.h"
#include "scscptags.h"

   
/*-----------------------------------------------------------------*/
/*! constructor
*/
/*-----------------------------------------------------------------*/
int SCSCP_callid_init(SCSCP_call_id *callid)
{
 callid->m_ID= callid->m_WritableID = NULL;
 return 1;
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
int SCSCP_callid_clear(SCSCP_call_id *callid)
{
 if (callid->m_WritableID) free(callid->m_WritableID);
 return 1;
}

/*-----------------------------------------------------------------*/
/*! write the object to output stream
   @param stream (inout) output stream
  @param callid (in) call id
  @param status (inout) error code
*/
/*-----------------------------------------------------------------*/
int SCSCP_callid_write(SCSCP_io *stream, const SCSCP_call_id* callid,  SCSCP_status* status)
{
 return SCSCP_io_writePairOMSOMSTR(stream, SCSCP_TAGS_cdname, SCSCP_TAGS_call_id , callid->m_ID, status);
}


/*-----------------------------------------------------------------*/
/*! read the object from th input stream
  @param stream (inout) input stream
  @param iter (inout) iterator on the input stream
*/
/*-----------------------------------------------------------------*/
int SCSCP_callid_read(SCSCP_xmlnodeptr* node, SCSCP_call_id* callid,  SCSCP_status* status)
{
 return SCSCP_xmlnode_readpairOMSOMSTR(node, SCSCP_TAGS_cdname, SCSCP_TAGS_call_id , &callid->m_ID);
}

/*-----------------------------------------------------------------*/
/*! return the string buffer of the call id
  @param callid (in) call id
*/
/*-----------------------------------------------------------------*/
const_SCSCP_string SCSCP_callid_get(const SCSCP_call_id *callid)
{
 return callid->m_ID;
}

/*-----------------------------------------------------------------*/
/*! return the string buffer of the call id
  @param callid (in) call id
  @param buffer (inout) new value (not duplicated and destroyed by callid)
*/
/*-----------------------------------------------------------------*/
void SCSCP_callid_setwritable(SCSCP_call_id *callid, char *buffer)
{
  SCSCP_callid_set(callid, NULL);
  callid->m_ID = callid->m_WritableID = buffer;
}

/*-----------------------------------------------------------------*/
/*! return the string buffer of the call id
  @param callid (in) call id
  @param buffer (in) new value (not duplicated)
*/
/*-----------------------------------------------------------------*/
void SCSCP_callid_set(SCSCP_call_id *callid, const char *buffer)
{
 if (callid->m_WritableID) free(callid->m_WritableID);
 callid->m_WritableID = NULL;
 callid->m_ID  = buffer;
}

