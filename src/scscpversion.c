/*-----------------------------------------------------------------*/
/*! 
  \file scscpversion.c
  \brief SCSCP version functions
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2012,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#include <stdio.h>
#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscputil.h"

/*-----------------------------------------------------------------*/
/*! return the SCSCP library version as string */
/*-----------------------------------------------------------------*/
const char* SCSCP_get_libversionstr()
{
	return SCSCP_STRINGIFY(SCSCP_VERSION_MAJOR) "." SCSCP_STRINGIFY(SCSCP_VERSION_MINOR) "." SCSCP_STRINGIFY(SCSCP_VERSION_PATCH) ;
}

/*-----------------------------------------------------------------*/
/*! return the SCSCP library version as numbers 
 @param major (out) major version
 @param minor (out) minor revision 
 @param patch (out) patch level 
 */
/*-----------------------------------------------------------------*/
void SCSCP_get_libversionnum(unsigned int *major, unsigned int *minor, unsigned int *patch)
{
 if (major) *major = SCSCP_VERSION_MAJOR;
 if (minor) *minor = SCSCP_VERSION_MINOR;
 if (patch) *patch = SCSCP_VERSION_PATCH;
}

/*-----------------------------------------------------------------*/
/*! check the SCSCP library compatibility 
 return 0 if library are not compatible
 return 1 if library are compatible and are equal
 return 2 if library are compatible and are not equal 
 @param major (in) major value to check
 @param minor (in) minor value to check
 @param patch (in) patch value to check
 */
/*-----------------------------------------------------------------*/
int SCSCP_check_libversion(unsigned int major, unsigned int minor, unsigned int patch)
{
    int ret = 0;
    if (major<SCSCP_VERSION_MAJOR) ret = 2;
    if (major==SCSCP_VERSION_MAJOR && minor<=SCSCP_VERSION_MINOR) ret = 2;
    if (major==SCSCP_VERSION_MAJOR && minor==SCSCP_VERSION_MINOR && patch==SCSCP_VERSION_PATCH) ret = 1;
    return ret;
}
